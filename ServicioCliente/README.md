# Servicio Cliente
Este servicio contiene información del cliente la cual es accesible a través de un api REST, el api es accesible en la dirección **http://localhost:8081/**

## Recursos

### /cliente/&laquo;id_cliente&raquo;

Este recurso trabaja bajo el verbo http **GET** y recibe el &laquo;id_cliente&raquo; del cliente en la url y responde con un json de la siguiente manera:

    {
        id: id
        nombre: nombre,
        NIT: num_nit
    }

Donde:

* **id:** es un numero entero que representa el identificador del cliente
* **nombre:** es el nombre del cliente que se identifica con el id recibido.
* **NIT:** es el número de identificación tributaria que le pertenece al cliente en cuestión.
 


