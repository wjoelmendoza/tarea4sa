#!/bin/python3

from flask import Flask
from flask_restful import Resource, Api

CLIENTES = {
            0: {'nombre': 'Carlos Estrada', 'NIT': '12548046'},
            1: {'nombre': 'Andrea Lopez', 'NIT': '12345610'},
            2: {'nombre': 'Andres Mencos', 'NIT': '12345611'},
            3: {'nombre': 'Monica Gallardo', 'NIT': '12345612'},
            4: {'nombre': 'Elver Suarez', 'NIT': '12345613'},
            5: {'nombre': 'Byron Elias', 'NIT': '12345614'},
            6: {'nombre': 'Erick Barrondo', 'NIT': '12345615'},
            7: {'nombre': 'Alejanda Burgos', 'NIT': 'C/F'},
            8: {'nombre': 'Elisa Alvarado', 'NIT': 'C/F'},
            9: {'nombre': 'Sergio Cortes', 'NIT': '12345616'},
        }


class Cliente(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar
    recursos del Api REST según el verbo que se le índique
    """

    def __init__(self):
        """"
        En este constructor se definen los conductores
        que se encuentran registrados
        """
        Resource()
        '''
        coleccion de conductores y carros
        '''
        self

    def get(self, id_cliente):
        """
        Se encarga de recuperar un cliente basado en el id que esta recibiendo
        :param id_cliente:
        :return: un diccionario con la informacion recolectada
        de la siguiente forma
        {
            'nombre': nombre_piloto,
            'NIT': num_nit 
        }
        """
        print("Solicitando datos del cliente")
        input("presiona una tecla para continuar")
        id_cliente = int(id_cliente)
        cliente = CLIENTES.get(id_cliente, None)

        datos = {
            'id_cliente': id_cliente,
            'nombre': cliente['nombre'],
            'NIT': cliente['NIT']
        }

        return datos


class Servidor:
    """
    Esta clase se encarga de inicializar el servidor y sus recursos
    REST
    """
    def __init__(self):
        """
        Inicializa el servidor web y sus correspondientes recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Cliente, '/cliente/<id_cliente>')

    def iniciar(self):
        """
        Inicia el servidor web y sus recursos
        """

        '''
        host = 0.0.0.0. escuchar en todas las interfaces de red
        port = 8081, puerto en el que escucha
        debug = True, indica que el servidor esta en pruebas
        '''
        self.app.run(host='0.0.0.0', port=8081, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
