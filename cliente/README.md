# Cliente
Este es un cliente de terminal el cual solicita:

* **id_cliente:** es el identificador del cliente que realizará la orden.
* **id_platillo:** es el identificador del platillo que solicita el cliente 
* **direccion** es la dirección donde debe entregarse la orden

Con estos datos se encarga de realizar un pedido al servicio de restaurante una vez realizado el pedido lo muestra en la terminal con el formato definido en el [Servicio Restaurante](ServicioRestaurante/README.md).
