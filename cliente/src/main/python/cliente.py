#!/bin/python3
from requests import post
import json


class Usuario:

    def ingresar_dato(self):
        """
        Solicita un id de usuario para realizar la peticion al servicio
        Se solicita el id de menu que se desea ordenar
        Se solicita una dirección de destino
        :return: None
        """

        try:
            entrada = input("Ingresa un id: ")
            menu = input("Ingresa el Id menú: ")
            direc = input("Ingrese direccion: ")

            while True:
                self.hacer_pedido(entrada, menu, direc)
                entrada = input("Ingresa un id: ")
                menu = input("Ingres Id menú: ")
                direc = input("Ingrese direccion: ")

        except EOFError:
            print("")
            return

    def hacer_pedido(self, id_cliente, id_platillo, direccion):
        """
        Se encarga de realizar la solicitud al ServicioRestaurante segun el
        los datos recibidos
        :param id_cliente: id del cliente que esta realizando el
        :param id_platillo: id del platillo que se desea ordenar
        :param direccion: la direccion a donde se va enviar el pedido
        :return: None
        """
        conexion = "http://localhost:8080/pedido"

        dt_cliente = {
            'id': int(id_cliente),
            'id_platillo': int(id_platillo),
            'direccion': direccion
        }

        dt = post(conexion, data=dt_cliente)
        vl = dt.json()

        print("datos del pedido")
        print(json.dumps(vl, indent=4))
        print("")


if __name__ == "__main__":
    usr = Usuario()
    usr.ingresar_dato()
