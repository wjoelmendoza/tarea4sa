from pybuilder.core import use_plugin, init

use_plugin("python.core")
use_plugin("python.install_dependencies")
use_plugin("python.flake8")
use_plugin("python.distutils")


name = "cliente"
version = "3.0.3"
default_task = ["publish", "analyze"]


@init
def set_properties(project):
    project.set_property("flake8_verbose_output", True)
    project.set_property("flake8_include_scripts", True)
