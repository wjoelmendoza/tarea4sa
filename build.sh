#!/bin/bash
#!/bin/bash
echo "instalando paquetes"
#su -
#apt-get update -qy
#apt-get install -y python3-dev python3-pip zip
#pip3 install --upgrade pip
#pip3 install --pre pybuilder

echo "generando artefactos"
mkdir -p dist/artefacto

# solo para prueba local
source env/bin/activate

cd cliente
pyb
cp target/dist/cliente*/dist/cliente* ../dist/artefacto/
cd ..
cd ESB
pyb
cp target/dist/ESB*/dist/ESB* ../dist/artefacto/
cd ..
cd ServicioCliente
pyb
cp target/dist/Servicio*/dist/Servicio* ../dist/artefacto/
cd ..
cd ServicioRepartidor
pyb
cp target/dist/Servicio*/dist/Servicio* ../dist/artefacto
cd ..
cd ServicioRestaurante
pyb
cp target/dist/Servicio*/dist/Servicio* ../dist/artefacto
cd ../dist

echo "generando el zip..."
zip artefacto.zip artefacto/*
#rm -r artefacto

echo "proceso terminado"
# solo para prueba local
deactivate
