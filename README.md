# Tarea2 SA
En esta tarea se requiere simular la coreografia de microservicio las tecnologías utilizadas son:

* __python 3.7.6__: Es un lenguaje de programación libre, su sitio [web](https://www.python.org/)
* __flask 1.1.1__: Es un microframework que permite crear sevidores web ligeros sobre el lenguaje python, este es su sitio [web](https://palletsprojects.com/p/flask/)
* __flask-restful  0.3.7__: Esta tecnología añade soporte para crear apis basadas en REST sobre el mircoframework [flask](https://flask-restful.readthedocs.io/en/latest/)
* __pybuilder 0.12__: se utiliza la versión en desarrollo de esta herramienta para construir el paquete de distribución de software, este es su sitio web [pybuilder](http://pybuilder.github.io/)
* __Netlify__: Se utilizo para desplegar el artefacto.

El estilo aplicado al código se basa en __pep8__

Se reestructuro las carpetas y el código fuente para poder utilizar pybuilder para la construcción de los artefactos.

![Diagrama](img/tarea3.jpg)

* diagrama de componentes *

## Descripción

El proyecto se compone de 3 apis REST y un cliente

* [Servicio Cliente](ServicioCliente/README.md): Este servicio se encarga de gestionr la información de los clientes.
* [Servicio Repartidor](ServicioRepartidor/README.md): Este servicio se encarga de asignar pilotos cuando el restaurante lo solicita para enviar un pedido al cliente.
* [Servicio Restaurante](ServicioRestaurante/README.md): Este servicio se encarga de recibir los pedidos del cliente, recupera sus datos del servicio de cliente y solicita un repartidor.
* [cliente](cliente/README.md): Es un programa de termina que consume el Servicio Restaurante.
* [ESB](ESB/README.md): Se encarga de coordinar los servicios.

demostración

[video](https://youtu.be/5tlqOywBWu4)
[artefacto](https://quizzical-cori-e50a24.netlify.com/)
