# Servicio Restaurante
Este servicio se encarga de gestionar los platillos y tiene la capacidad de recibir ordenes de parte del cliente.
El api REST se expone en **http://localhost:8083**.

## Recursos

### /pedido/

Este recurso esta disponible mediante el verbo **POST** recibe un objeto json con el siguiente formato

    {
        'id': id_cliente,
        'id_platillo': id_platillo,
        'direccion': dirección
    }

Donde:

* **id:** es un valor entero que representa el identificador del cliente que esta realizando el pedido.
* **id_platillo:** es un valor entero que representa el identificador del platillo que el cliente esta solicitando.
* **direccion:** es una cadena que representa la direccion donde se debe entregar la orden

Se recopila la información del cliente, del menú y el repartidor

    {
        'cliente': datos_cliente,
        'menu': desc_menu,
        'repartidor': datos_repartidor
    }

Donde:

* **cliente:** Es un objeto json que contiene la información del cliente y esta definido en [Servicio cliente](ServicioCliente/README.md).
* **desc:** Es una cadena que describe el platillo que solicito el cliente.
* **repartidos:** Es un objeto json que contiene la información del repartidor y el destino de la orden se encuentra definido en [ServicioRepartidor](ServicioRepartidor/README.md). 
