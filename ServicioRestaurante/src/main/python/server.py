#!/bin/python3
from flask import Flask
from flask_restful import Resource, Api, reqparse
from requests import get


MENUES = {
            0: 'Pollo en crema',
            1: 'Pizza Hawaina',
            2: 'Caldo de costilla',
            3: 'Frijoles Blancos ',
            4: 'Frijoles colorados con Chicharron',
            5: 'Tacos al pastor',
            6: 'Carne Azada',
            7: 'Caldo de Gallina',
            8: 'Pollo Frito con papas fritas',
            9: 'Costilla a la barbacoa'
        }


class Restaurante(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar
    los recursos del api REST segun el verbo que se le indique
    """

    def __init__(self):
        """
        En este constructor se define la coleccion
        de carros validos para el servicio
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id_cliente', type=int)
        self.parser.add_argument('nombre_cliente', type=str)
        self.parser.add_argument('NIT', type=str)
        self.parser.add_argument('id_piloto', type=int)
        self.parser.add_argument('nombre_piloto', type=str)
        self.parser.add_argument('vehiculo', type=str)
        self.parser.add_argument('destino', type=str)
        self.parser.add_argument('id_platillo', type=int)

    def post(self):
        """
        Esta recurso recibe un json con el formato
        {
            'id': id_cliente,
            'id_platillo': id_platillo,
            'direccion': dirección
        }
        :param id_carro:
        :return: un diccionario con la informacion de la orden con el siguiente
        formato:
        {
            'cliente': datos_cliente,
            'menu': desc_menu,
            'repartidor': datos_repartidor
        }
        """
        print("solicitando comida")
        input("presiona una tecla para continuar")
        datos = self.parser.parse_args()

        id_menu = int(datos['id_platillo'])

        menu = MENUES[id_menu]

        datos['menu'] = menu

        return datos

    def get_cliente(self, id_cliente):
        """
        Se encarga de obtener la información del cliente
        :param id_cliente: es el id del cliente que realizá el pedido
        :return: la información del cliente consultada al servidor
        de cliente
        """
        recurso = 'http://localhost:8081/cliente/' + str(id_cliente)
        rst = get(recurso).json()
        return rst

    def get_repartidor(self, direccion):
        """
        Se encarga de requerir un repartidor para la entrega de la orden del
        cliente donde le envia la dirección de destino de la orden.
        :param direccion: es la dirección en la que el cliente esper su orden
        :return: diccionario con los datos del repartidor
        """
        recurso = 'http://localhost:8082/repartidor/' + direccion
        rst = get(recurso).json()
        return rst


class Servidor:
    """
    Esta clase se encarga de inicializar las diferentes
    herramientas para crear el api rest
    """
    def __init__(self):
        """
        crear el servidor web junto con los recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Restaurante, '/pedido')

    def iniciar(self):
        """
        Se encarga de iniciar el servidor web
        :return: None
        """
        '''
        host = 0.0.0.0 indica que escuche en todas las interfaces de red
        port = 8083 indica en que puerto tiene que escuchar
        debug = True permite mensajes de error mas especificos los cuales
        se muestan en la consola
        '''
        self.app.run(host='0.0.0.0', port=8083, debug=True)


if __name__ == '__main__':
    server = Servidor()
    server.iniciar()
