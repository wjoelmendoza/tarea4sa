# ESB

Se encarga de coordinar los diferentes servicios de los que se compone
la aplicación.

El api REST se expone en **http://localhost:8080**.

## Recursos

### /pedido

Este recurso esta disponible mediante el verbo **POST** recibe un objeto json con el siguiente formato

    {
        'id': id_cliente,
        'id_platillo': id_platillo,
        'direccion': dirección
    }

* **id:** es un valor entero que representa el identificador del cliente que esta realizando el pedido.
* **id_platillo:** es un valor entero que representa el identificador del platillo que el cliente esta solicitando.
* **direccion:** es una cadena que representa la direccion donde se debe entregar la orden

### /cliente/&laquo;id_cliente&raquo;

Este recurso trabaja bajo el verbo http **GET** y recibe el &laquo;id_cliente&raquo; del cliente en la url redirige la peticion
al servidor del Cliente y responde con el siguiente json:

    {
        id: id
        nombre: nombre,
        NIT: num_nit
    }

Donde:

* **id:** es un numero entero que representa el identificador del cliente
* **nombre:** es el nombre del cliente que se identifica con el id recibido.
* **NIT:** es el número de identificación tributaria que le pertenece al cliente en cuestión.

### /repartidor/&laquo;direccion&raquo;

Este recurso utiliza el verbo http **GET**, recibe la &laquo;direccion&raquo; de destino, se encarga de consumir
al servido del Repartidor y retorna la información del repartidor con el siguiente formato json:

    {
        id: id_piloto,
        nombre: nombre,
        vehiculo: vehiculo
        destino: direccion
    }

Donde:

* **id:** Es un valor entero que representa el identifica al piloto asignado.
* **nombre:** Es una cadena que representa el nombre del piloto asignado.
* **vehiculo:** Es una cadena que representa el modelo del vehículo asignado.
* **destino:** Es una cadena que representa la dirección donde debe ser entregada la orden.
