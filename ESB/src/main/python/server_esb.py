#!/bin/python3
from flask import Flask
from flask_restful import Resource, Api, reqparse
from requests import post, get


class Recursos(object):
    """
    Esta clase se encarga de consumir los recursos de los servicios
    """

    def get_repartidor(self, direccion):
        """
        Consume el recurso repartidor del servidor de Repartidor
        """
        recurso = 'http://localhost:8082/repartidor/' + direccion
        rst = get(recurso).json()
        return rst

    def get_cliente(self, id_cliente):
        """
        Consume el recurso cliente del servidor de Cliente
        """
        recurso = 'http://localhost:8081/cliente/' + str(id_cliente)
        rst = get(recurso).json()
        return rst


class Repartidor(Resource):
    """
    Hereda de la clase Resourse y se encarga de exponer el servidor
    de Repartidor
    """

    def get(self, direccion):
        """
        Expone el recurso para retornar un repartidor
        """
        rcs = Recursos()
        return rcs.get_repartidor(direccion)


class Cliente(Resource):
    """
    Hereda de la clase Resourse y se encarga de exponer el servidor
    de Cliente
    """
    def get(self, id_cliente):
        rcs = Recursos()
        return rcs.get_cliente(id_cliente)


class Restaurante(Resource):
    """
    Hereda de la clase Resourse y se encarga de exponer el servidor
    de Restaurante
    """

    def __init__(self):
        """
        Constructor de la clase, inicializa un parser para poder leer
        los parametros del request
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=int)
        self.parser.add_argument('id_platillo', type=int)
        self.parser.add_argument('direccion', type=str)

    def post(self):
        """
        Se encarga de exponer la orden del restaurante,
        hace el requerimiento del cliente y el repartidor
        y por último hace la solicitud del menu
        """
        datos = self.parser.parse_args()

        # consumiendo el servicio de repartidor
        rcs = Recursos()
        repartidor = rcs.get_repartidor(datos['direccion'])

        # consumiendo el servicio de cliente
        cliente = rcs.get_cliente(datos['id'])

        envio = {
            'id_cliente': cliente['id_cliente'],
            'nombre_cliente': cliente['nombre'],
            'NIT': cliente['NIT'],
            'id_piloto': repartidor['id_piloto'],
            'nombre_piloto': repartidor['nombre'],
            'vehiculo': repartidor['vehiculo'],
            'destino': repartidor['destino'],
            'id_platillo': datos['id_platillo']
        }

        # realizando pedido
        recurso = "http://localhost:8083/pedido"
        pedido = post(recurso, data=envio).json()

        return pedido


class Servidor:
    """
    Se encarga de encapsular el servidor de flask
    """

    def __init__(self):
        """
        Este constructor instancia un servidor flask, crea un api rest
        y expone los diferentes recursos
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Repartidor, '/repartidor/<direccion>')
        self.api.add_resource(Cliente, '/cliente/<id_cliente>')
        self.api.add_resource(Restaurante, '/pedido')

    def iniciar(self):
        """
        Ejecutar el servidor para que escuche el puerto 8080
        """
        self.app.run(host='0.0.0.0', port=8080, debug=True)


if __name__ == '__main__':
    # si el script es el principal ejecuta el servidor
    server = Servidor()
    server.iniciar()
